#!/bin/sh
mkdir target
LIBS=$(ls -1 lib/*jar|tr '\n' ':')
javac -cp $LIBS -d target `find src -name "*.java"`
java -cp target:$LIBS:src amidst/Amidst
